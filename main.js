import Vue from 'vue'
import App from './App'
import http from '@/utils/http.js'
import uView from "@/uni_modules/uview-ui";
Vue.use(uView);
Vue.prototype.$http=http
Vue.prototype.$timeStamp = function(val) {
	let times = Math.round(new Date(val) / 1000)
	return times
}

Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
    ...App
})
app.$mount()
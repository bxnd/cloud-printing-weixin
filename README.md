# 校园云打印系统微信小程序

## 介绍
本小程序使用uni-app+uview-ui进行开发。

## 安装教程

1. 拉取本项目，使用HBuilderX打开，把appid修改成自己的。
2. 安装插件，点击工具-》点击安装插件-》安装scss/sass编译。
3. 打开Dcloud插件市场官网[https://ext.dcloud.net.cn/](https://ext.dcloud.net.cn/),给项目导入uView2.0、uni-file-picker、uni-rate插件,此时项目的目录为。

![项目目录](images/mulv.png)

4. 然后点击运行-》运行到小程序模拟器-》微信开发者功能,即可正常运行。

## 系统展示
### 1登录模块
![项目目录](images/图片1.png)
![项目目录](images/图片2.png)
### 2首页
![项目目录](images/图片3.png)
![项目目录](images/图片4.png)
![项目目录](images/图片5.png)
![项目目录](images/图片6.png)
![项目目录](images/图片7.png)
![项目目录](images/图片8.png)
### 3订单
![项目目录](images/图片9.png)
![项目目录](images/图片10.png)
![项目目录](images/图片11.png)
![项目目录](images/图片12.png)
### 4我的
![项目目录](images/图片13.png)
![项目目录](images/图片14.png)
![项目目录](images/图片15.png)
![项目目录](images/图片16.png)
